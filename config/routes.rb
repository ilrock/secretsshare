Rails.application.routes.draw do
  resource :wizard do
    get :step1
    get :step2
    get :step3
    get :step4

    post :validate_step
  end
  root to: 'secrets#recent'
  
  resources :secrets, except: [:show]
  resources :sessions, only: [:create, :new, :destroy]
  resources :comments
  
  post '/secret/upvote', to: "secrets#upvote"
  post '/secret/downvote', to: "secrets#downvote"
  post '/secret/report', to: "secrets#report"
  get '/secret/random', to: "secrets#get_random_secret"
  get '/admin', to: "secrets#admin"
  get '/secrets/recent', to: "secrets#most_popular"
end

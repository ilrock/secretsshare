class WizardsController < ApplicationController
  before_action :load_secret_wizard, except: %i(validate_step)

  def validate_step
    current_step = params[:current_step]

    @secret_wizard = wizard_secret_for_step(current_step)
    @secret_wizard.secret.attributes = secret_wizard_params
    session[:secret_attributes] = @secret_wizard.secret.attributes

    if @secret_wizard.valid?
      next_step = wizard_secret_next_step(current_step)
      create and return unless next_step

      redirect_to action: next_step
    else
      render current_step
    end
  end

  def create
    if @secret_wizard.secret.save
      session[:secret_attributes] = nil
      redirect_to root_path, notice: 'Secret succesfully created!'
    else
      redirect_to({ action: Wizard::Secret::STEPS.first }, alert: 'There were a problem when creating the secret.')
    end
  end

  private

  def load_secret_wizard
    @secret_wizard = wizard_secret_for_step(action_name)
  end

  def wizard_secret_next_step(step)
    Wizard::Secret::STEPS[Wizard::Secret::STEPS.index(step) + 1]
  end

  def wizard_secret_for_step(step)
    raise InvalidStep unless step.in?(Wizard::Secret::STEPS)

    "Wizard::Secret::#{step.camelize}".constantize.new(session[:secret_attributes])
  end

  def secret_wizard_params
    params.require(:secret_wizard).permit(:user_gender, :user_age, :secret_body, :category_id)
  end

  class InvalidStep < StandardError; end
end
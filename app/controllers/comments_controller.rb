class CommentsController < ApplicationController
  def create
    secret = Secret.find(params[:secret])
    comment_body = params[:body]
    comment = Comment.create(body: comment_body)
    secret.comments.push(comment)

    if comment.save
      respond_to do |format|
        format.json { 
          render json: {:status => "ok", :comment => comment }
        }
      end
    else
      respond_to do |format|
        format.json { 
          render json: {:status => "nok" }
        }
      end
    end
  end
end
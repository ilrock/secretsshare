class SessionsController < ApplicationController
  def new
    @user = User.new
  end

  def create
    # sleep 2 # you can add sleep here  if you want to  slow down brute force attack
              # for normal application this is bad idea but for one
              # user login no-one care

    user_params = params.require(:user)

    @user = User.new
      .tap { |su| su.username = user_params[:username] }
      .tap { |su| su.password = user_params[:password] }

    if @user.login_valid?
      session[:current_user] = true
      redirect_to '/admin'
    else
      @user.password = nil
      flash[:notice] = 'Sorry, wrong credentils'
      render 'new'
    end
  end

  def destroy
    reset_session
    redirect_to root_path
  end
end
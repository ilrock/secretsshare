class SecretsController < ApplicationController
  before_action :check_if_authenticated, only: [:admin, :destroy]
  def most_popular
  	@secrets = Secret.all
  	@secrets = (@secrets.sort_by { |a| a.upvotes }).reverse!
    @secret = Secret.find_by_id(params[:secret]) if params[:secret]
  end

  def admin
    @secrets = Secret.where(:reported => true)
  end

  def report
    secret = Secret.find_by_id(params[:secret])
    secret.reported = true
  
    if secret.save
      respond_to do |format|
        format.json { 
          render json: {:status => "ok" }
        }
      end
    else
      respond_to do |format|
        format.json { 
          render json: {:status => "nok" }
        }
      end
    end
  end

  def index
    if params[:category]
      @secrets = Category.find_by_name(params[:category]).secrets
    else
      @secrets = Secret.all
    end

    @secrets = (@secrets.sort_by { |a| a.upvotes }).reverse!
  end

  def recent
    @secrets = Secret.all.order('created_at DESC')
  end

  def get_random_secret
    secret = Secret.all[(rand(0..Secret.all.length - 1))]
    if secret
      status == "ok"
    else
      status == "nok"
    end
    respond_to do |format|
      format.json { 
        render json: {:status => status, :secret => secret }
      }
    end
  end

  def upvote
  	secret = Secret.find_by_id(params[:secret])
  	if !cookies[:upvoted].to_s.include?("secret-#{secret.id}")
  	  secret.update_attributes(:upvotes => secret.upvotes+1)
  	  cookies[:upvoted] = "#{cookies[:upvoted]}secret-#{secret.id};"
  	  status = "ok"
    else
      status = "nok"
    end
    respond_to do |format|
      format.json { 
        render json: {:status => status, :upvotes_value => secret.upvotes }
      }
    end
  end

  def downvote
  	secret = Secret.find_by_id(params[:secret])
  	if !cookies[:downvoted].to_s.include?("secret-#{secret.id}")
  	  secret.update_attributes(:upvotes => secret.upvotes-1)
  	  cookies[:downvoted] = "#{cookies[:downvoted]}secret-#{secret.id};"
  	  status = "ok"
    else
      status = "nok"
    end
    respond_to do |format|
      format.json { 
        render json: {:status => status, :upvotes_value => secret.upvotes }
      }
    end
  end

  def destroy
    Secret.find_by_id(params[:id]).destroy

    respond_to do |format|
      format.html { redirect_back }
    end
  end

  private

  def check_if_authenticated
    unless session[:current_user]
      redirect_to '/'
    end
  end
end

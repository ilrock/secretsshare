module Wizard
  module Secret
    STEPS = %w(step1 step2 step3 step4).freeze

    class Base
      include ActiveModel::Model
      attr_accessor :secret

      delegate *::Secret.attribute_names.map { |attr| [attr, "#{attr}="] }.flatten, to: :secret

      def initialize(secret_attributes)
        @secret = ::Secret.new(secret_attributes)
      end
    end

    class Step1 < Base
      validates :user_gender, presence: true
    end

    class Step2 < Step1
      validates :user_age, presence: true
    end

    class Step3 < Step2
      validates :secret_body, presence: true
    end

    class Step4 < Step3
      validates :category_id, presence: true
    end
  end
end
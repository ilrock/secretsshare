$(document).ready(function(){
	$('.change-upvotes').on("click", function(){
		var secret_id = $(this).attr("data-secret");
		if ($(this).attr("data-action") == "upvote"){
			var url = "/secret/upvote?secret=" + secret_id;
		} 
		else {
			var url = "/secret/downvote?secret=" + secret_id;
		}
		$.ajax({
			method: "POST",
		  	url: url,
		  	success: function(data){
		  		console.log(data);
		  		if (data.status == "ok"){
		  			var upvotes = $("#upvotes-" + secret_id);
		  			console.log(upvotes);
		  			upvotes.text(data.upvotes_value);
		  		}
		  	}
	  	})
	});

	$('.show-comments').on("click", function(){
		$(this).parent().next().toggleClass("hidden");
	});

	$('.send-comment').on("click", function(){
		var btn = $(this)
		var secret = btn.parents('.card').attr("data-secret");
		var input = btn.parents(".row").first().find('input').first()
		var body = input.val()
		var comments_row = btn.parents(".row").first().prev()
		if (body != ""){
			$.ajax({
				method: "POST",
			  	url: "/comments?secret="+ secret + "&body=" + body,
			  	success: function(data){
			  		console.log(data);
			  		if (data.status == "ok"){
			  			// console.log($(this).parents(".row").first().prev());
			  			var comment_html = '<img src="https://api.adorable.io/avatars/100/comment-"'+ data.comment.id +' style="height: 30px; width: auto; border-radius: 50%; margin-left: 15px;">	<span style="padding-left: 15px; border-left: 1px solid #ededed;"> ' + data.comment.body + ' </span> <span class="pull-right" style="font-size: 14px; color: #cdcdcd;"> Just now </span>'
			  			comments_row.append("<div class='col-12' style='margin-bottom: 10px'> "+ comment_html +" </div>");
			  			input.val("");
			  		}
			  	}
		  	})
		 } else{
		 	
		 }


	});
});
class AddUpvotesToSecrets < ActiveRecord::Migration[5.1]
  def change
  	add_column :secrets, :upvotes, :integer, default: 0, nil: 0
  end
end

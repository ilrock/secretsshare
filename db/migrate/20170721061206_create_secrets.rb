class CreateSecrets < ActiveRecord::Migration[5.1]
  def change
    create_table :secrets do |t|
      t.string :user_gender
      t.integer :user_age
      t.text :secret_body
      t.references :category
      t.timestamps
    end
  end
end

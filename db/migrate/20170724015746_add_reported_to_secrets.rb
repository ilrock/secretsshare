class AddReportedToSecrets < ActiveRecord::Migration[5.1]
  def change
  	add_column :secrets, :reported, :boolean, :nil => false, :default => false
  end
end

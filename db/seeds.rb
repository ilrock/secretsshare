# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Category.create([
  {
  	name: "funny",
  	icon: "//farm5.staticflickr.com/4309/36061871775_078e10cb50_o.png"
  },
  {
  	name: "sexy",
  	icon: "//farm5.staticflickr.com/4315/36061872245_d3cac45b3d_o.png"
  },
  {
  	name: "love&relationships",
  	icon: "//farm5.staticflickr.com/4304/36061872455_643929081e_o.png"
  },
  {
  	name: "rant",
  	icon: "//farm5.staticflickr.com/4317/36061872085_10fb4a7b65_o.png"
  },
  {
  	name: "embarassing",
  	icon: "//farm5.staticflickr.com/4310/36061871525_9ce6c7d13a_o.png"
  },
  {
  	name: "miscellaneous",
  	icon: "//farm5.staticflickr.com/4324/36061871925_4d069a4326_o.png"
  }
])

# Secret.create([
#   {
#     user_gender: "man",
#     user_age: "20",
#     secret_body: "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.",
#   	category_id: 1
#   },
#   {
#     user_gender: "woman",
#     user_age: "20",
#     secret_body: "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.",
#   	category_id: 2
#   },
#   {
#     user_gender: "man",
#     user_age: "20",
#     secret_body: "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.",
#   	category_id: 3
#   },
#   {
#     user_gender: "woman",
#     user_age: "20",
#     secret_body: "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.",
#   	category_id: 4
#   },
#   {
#     user_gender: "man",
#     user_age: "20",
#     secret_body: "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.",
#   	category_id: 5
#   },
#   {
#     user_gender: "woman",
#     user_age: "20",
#     secret_body: "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.",
#   	category_id: 6
#   }
# ])